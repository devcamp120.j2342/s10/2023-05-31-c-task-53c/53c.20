import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(0, 0);
        MovablePoint point2 = new MovablePoint(10, 10);

        System.out.println(point1.toString());
        System.out.println(point2.toString());

        point1.moveDown();
        point2.moveUp();

        System.out.println(point1.toString());
        System.out.println(point2.toString());

        point1.moveLeft();
        point2.moveRight();

        System.out.println(point1.toString());
        System.out.println(point2.toString());
    }
}
